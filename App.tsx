import { StatusBar } from "expo-status-bar";
import {
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  Alert,
  Switch,
  Platform,
  Image,
  ScrollView,
  FlatList,
  ActivityIndicator,
  Appearance,
} from "react-native";
import React, { useState, useEffect, createContext,
  //  useContext 
  } from "react";
import { useTranslation } from "react-i18next";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
  useTheme,
} from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import SwitchSelector from "react-native-switch-selector";
import Icon from "react-native-vector-icons/FontAwesome5";
import Style from "./styles";
// import { en, vn } from "./localizations";
import * as Localization from "expo-localization";
import I18n from "i18n-js";
import style from "./styles";
import * as ImagePicker from "expo-image-picker";
import { Constants } from "expo-constants";
import * as Location from "expo-location";
import Event from "react-native-event-listeners";
// import images from "./img/images.png";
import array from "./list";
import {
  Provider as PaperProvider,
  DarkTheme as PaperDarkTheme,
  Colors,
} from "react-native-paper";
import { useColorScheme } from "react-native";
import { EventRegister } from "react-native-event-listeners";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { useContext } from "react";
import { SheetProvider } from "react-native-actions-sheet";
import { ActionSheetProvider } from "@expo/react-native-action-sheet";
import ExampleScreen from './ExampleScreen'



const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();
const ExampleContext = React.createContext();

const App = () => {
  const [darkApp, setDarkApp] = useState(false);
  const appTheme = darkApp ? DarkTheme : DefaultTheme;
  // const TiengViet = useContext(dataContext);

  useEffect(() => {
    let eventListener = EventRegister.addEventListener(
      "changeThemeEvent",
      (data) => {
        // Alert.alert('true'),
        setDarkApp(data);
      }
    );
    return () => {
      EventRegister.removeEventListener(eventListener);
    };
  });
  return (
    <PaperProvider theme={DarkTheme}>
      
      <NavigationContainer
        theme={appTheme}
        // theme={DarkTheme }
        // theme={MyTheme}
      >
        <Stack.Navigator initialRouteName="TabScreenr">
          <Stack.Screen
            name="Login"
            component={LoginScreen}
            options={{ title: "" }}
          />
          <Stack.Screen
            name="TabScreen"
            component={TabScreen}
            options={{ title: "" }}
          />
          <Stack.Screen
            name="Moredata"
            component={Moredata}
            options={{ title: "" }}
          />
          <Stack.Screen
            name="Forgot"
            component={Forgot}
            options={{ title: "" }}
          />
          
        </Stack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  );
};

function MyTabBar({ state, descriptors, navigation }) {
  return (
    <View style={{ flexDirection: 'row' }}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({ name: route.name, merge: true });
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{ flex: 1,height:60,justifyContent:'center',alignItems:'center' }}
          >
            <Text style={{ color: isFocused ? '#673ab7' : 'blue' }}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const TabScreen = () => {

  return (
    <Tab.Navigator tabBar={props => <MyTabBar {...props} />}  >
      <Tab.Screen
        name="Contacts"
        component={ContactsScreen}
        options={{ headerShown: false }}
      ></Tab.Screen>

      <Tab.Screen
        name="Setting"
        component={SettingScreen}
        options={{ headerShown: false }}
      ></Tab.Screen>
    </Tab.Navigator>
  );
};
// const UselessTextInput = (props) => {
//   return <TextInput {...props} editable maxLength={40} />;
// };
const Forgot = () => {
  // const TiengViet = useContext(dataContext);
  const { colors } = useTheme();
  const [value, onChangeText] = React.useState("starling.vn@gmail.com");
  return (
    <View style={[style.Forgot, { backgroundColor: colors.card }]}>
      <View>
        <Text style={{ fontSize: 25, color: colors.text, padding: 10 }}>
          Forgot your passwd?
        </Text>
      </View>
      <View style={{ justifyContent: "center", alignItems: "center", flex: 2 }}>
        <TextInput
          style={{
            backgroundColor: "white",
            width: 350,
            height: 60,
            borderRadius: 10,
            padding: 10,
            borderColor: "black",
            borderWidth: 1,
          }}
          
          onChangeText={(text) => onChangeText(text)}
          value={value}
        />

        <TouchableOpacity
          style={{
            top: 40,
            width: 350,
            height: 60,
            borderRadius: 10,
            backgroundColor: "#CCC",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Text style={{ fontSize: 17, color: "white", fontWeight: "bold" }}>
            OK
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const LoginScreen = ({ navigation }) => {
  // const TiengViet = useContext(dataContext);
  const [email, setEmail] = useState("");
  const [passwd, setPasswd] = useState("");
  const [ishide, setHiden] = useState("");
  const { colors } = useTheme();
  //fdssssssssssssssssssssd
  const forgot = () => {
    navigation.navigate("Forgot");
  };
  const onpress = () => {
    if (email === "" && passwd === "") {
      navigation.navigate("TabScreen");
    } else {
      Alert.alert("Tài khoản hoặc mậtkhaaur khôgn đúng");
    }
  };
  // const { tie } = React.useContext(ExampleContext);


  return (
    <View style={[Style.container, { backgroundColor: Colors.card }]}>
      <KeyboardAwareScrollView
        contentContainerStyle={{ flexGrow: 1 }}
        showsVerticalScrollIndicator={false}
      >
        <View
          style={[Style.top, { backgroundColor: colors.card, height: 100 }]}
        >
          <Text style={[Style.Text, { color: colors.text }]}>
            Sign in to your account
          </Text>
        </View>

        <View style={[Style.input, { backgroundColor: colors.card }]}>
          <TextInput
            placeholder="Email"
            style={[Style.input12, { backgroundColor: "white" }]}
            onChangeText={setEmail}
            value={email}
            
          />
          <View style={[Style.input11, { backgroundColor: "white" }]}>
            <TextInput
              placeholder="Password"
              style={Style.input1}
              onChangeText={setPasswd}
              value={passwd}
              secureTextEntry={ishide ? true : false}
            />

            <TouchableOpacity
              style={{ top: 15 }}
              onPress={() => setHiden(!ishide)}
            >
              <Icon
                style={{ fontSize: 15 }}
                name={ishide ? "eye-slash" : "eye"}
                backgroundColor="#3b5998"
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={forgot}>
            <Text style={[Style.ViewText, { color: colors.text }]}>
              Forgot your password
            </Text>
          </TouchableOpacity>
          <View style={Style.button}>
            <View style={Style.abcd}>
              <Text style={Style.textStyle} onPress={onpress}>
                Sign in
                {/* {!TiengViet ? "Đăng Nhập" : "Sign In"} */}
              </Text>
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
};

// const { colors } = useTheme();

const Item = (
  { title, title2, color, text } 
) => (
  <View style={{ backgroundColor: color, padding: 20,height:60 }}>
    <Text style={{ fontSize: 18, left: -30, color: text,top:-10}}>{title}</Text>
    <Text style={{ left: -30, color: "#767577" }}>{title2}</Text>
  </View>
);

const ContactsScreen = ({ navigation }) => {
  // const TiengViet = useContext(dataContext);

  const { colors } = useTheme();

  const renderItem = ({ item }) => (
    <View
      style={{
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 13,
        
        
      }}
    >
      <View
        style={{
          borderRadius: 100,
          height: 15,
          width: 90,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Image
          source={item.image}
          style={{ height: 55, width: 55, borderRadius: 30 }}
        />
      </View>

      <Item
        color={colors.card}
        text={colors.text}
        title={item.item}
        title2={item.email}
      />
    </View>
  );
  // const renderItem2 = ({ item2 }) => <Item title={item2.email} />

  const Onpress = () => {
    navigation.navigate("Moredata");
  };
  // const itemSeparator = () => {
  //   return (
  //     <View style={{ height: 1, width: "100%", backgroundColor: "#CCC" }} />
  //   );
  // };
  const [TiengViet, setTiengViet] = useState(false);

  return (
    <View style={Style.containerContac}>
        <View style={[Style.ContacTop, { backgroundColor: colors.card }]}>
          <Text style={[Style.TextContac, { color: colors.text }]}>
            Your Contacs
            {/* {!TiengViet ? "Liên Hệ" : "Your Contacs"} */}
          </Text>
          <View style={[Style.abc, { backgroundColor: colors.card }]}>
            <TouchableOpacity onPress={Onpress} style={Style.icon}>
              <Text style={Style.btnContacs}  >
                + Add a new contact
                {/* {!TiengViet ? "+ Thêm liên hệ mới" : "+ Add a new contact"} */}
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <View
          style={[Style.ListContacs]}
        >
          <FlatList
          style={{backgroundColor:colors.card}} 
            data={array}
            renderItem={renderItem}
            // ItemSeparatorComponent={itemSeparator}
            keyExtractor={(item) => item.id}
          />
        </View>


      
    </View>
  );
};

// const dataContext = createContext();

const SettingScreen = ({ navigation }) => {
  const SignOut = () => {
    navigation.navigate("Login");
  };

  const createTwoButtonAlert = () =>
    Alert.alert("Sign Out", "Are you sure want to sign out now?", [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel",
      },
      { text: "OK", onPress: () => SignOut() },
    ]);

  const { colors } = useTheme();

  const [darkMode, setdrakMode] = useState(true);

  const [isEnabled, setIsEnabled] = useState(false);

  const [TiengViet, setTiengViet] = useState(false);

  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  
  const [ishide, setHiden] = useState(false);

  const [result, setResult] = useState("🔮");

  const abc  = () => {

  }
  

  return (
    // <ExampleContext.Provider value={{ TiengViet }}>
      <View style={[Style.containersetting, { backgroundColor: colors.card }]}>
        <View style={[Style.settingTop, { backgroundColor: colors.card }]}>
          <Text style={[Style.settingText, { color: colors.text }]}>
            {!TiengViet ? "Cài Đặt" : "Settings"}
          </Text>
        </View>
        <View style={Style.settingLanguage}>
          <TouchableOpacity
            onPress={() => setTiengViet(!TiengViet)}
           >
            <View>
              <View
                style={{
                  flexDirection: "row",
                  backgroundColor: colors.card,
                  height: 70,
                }}
              >
                <Text
                  style={[
                    Style.textLanguage,
                    { width: 350, top: 10, color: colors.text },
                  ]}
                >
                  <Icon
                    size={15}
                    name="cog"
                    backgroundColor="#2539FF"
                    color={"#2539FF"}
                  />
                  {!TiengViet ? "Ngôn ngữ" : "Language"}
                </Text>
                <View style={{ right: 60, top: 10 }}>
                  <Text style={{ fontSize: 17, color: colors.text }}>
                    {!TiengViet ? "English" : "Tiếng  Việt"}
                  </Text>
                </View>
              </View>
            </View>

            {/* <SheetProvider>
              <ExampleScreen />
            </SheetProvider> */}


          </TouchableOpacity>
        </View>

        <View
          style={
            //theme == 'light'?
            Style.settingThemes
            //  :darkMode.settingThemes
          }
        >
          <TouchableOpacity onPress={() =>{ setdrakMode(!darkMode)}}>
            <View
              style={{
                flexDirection: "row",
                backgroundColor: colors.card,
                height: 70,
              }}
            >
              <Text
                style={[Style.textThemes, { color: colors.text }]}
                onPress={() => setHiden(!ishide)}
              >
                <Icon
                  size={15}
                  name="cog"
                  backgroundColor="#3b5998"
                  color={"#2539FF"}
                />
                {!TiengViet ? "Giao Diện" : "Theme"}
              </Text>
              <View style={{ top: 15 }}>
                

                <Switch
                  style={{ bottom: 10, right: 30 }}
                  value={darkMode}
                  onValueChange={() => {
                    setdrakMode(!darkMode);
                    EventRegister.emit("changeThemeEvent", darkMode);
                  }}
                />
                {/* <Text  style={{ color: colors.text, fontSize: 17, right: 35 }}>
                {!ishide ? "Drak" : "Light"}
                </Text> */}
              </View>
            </View>
          </TouchableOpacity>
        </View>

        <View style={Style.settingNotifications}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              backgroundColor: colors.card,
              height: 70,
            }}
          >
            <Text style={[Style.textNotifications, { color: colors.text }]}>
              <Icon
                name="cog"
                size={15}
                backgroundColor="#3b5998"
                color={"#2539FF"}
              />

              {!TiengViet ? "Thông báo" : "Notifications"}
            </Text>
            <Switch
              style={{ right: 30 }}
              trackColor={{ false: "#767577", true: "#81b0ff" }}
              thumbColor={isEnabled ? "#2EE32D" : "#f4f3f4"}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
          </View>
        </View>

        <View style={Style.settingSignOut}>
          <TouchableOpacity>
            <View style={{ backgroundColor: colors.card, height: 70 }}>
              <Text
                style={[Style.textSignOut, { color: colors.text }]}
                title="Login"
                onPress={createTwoButtonAlert}
              >
                {!TiengViet ? "Đăng xuất" : "SignOut"}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    // </ExampleContext.Provider>
  );
};

const Moredata = () => {
  const [image, setImage] = useState("");

  const [location, setLocation] = useState(null);
  // const [value, onChangeText] = React.useState(location);
  // console.log(value,'asda');
  
  
  const [errorMsg, setErrorMsg] = useState(null);

  useEffect(() => {
    (async () => {
      // let { status } = await Location.requestForegroundPermissionsAsync();
      // if (status !== "granted") {
      //   setErrorMsg("Permission to access location was denied");
      //   return;
      // }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
      console.log('location', location)
    })();
  }, []);

  // let text = "Waiting..";

  // if (errorMsg) {
  //   text = errorMsg;
  // } else if (location) {
  //   text = JSON.stringify(location);
  // }

  let openImagePickerAsync = async () => {
    let pickerResult = await ImagePicker.launchImageLibraryAsync();
    console.log(pickerResult);
    setImage(pickerResult.uri);
  };
  return (
    <View style={style.ContainerModrdata}>
      <KeyboardAwareScrollView
        contentContainerStyle={{ flexGrow: 1 }}
        showsVerticalScrollIndicator={false}
      >
        <View style={style.MoredataText}>
          <Text style={style.MdtText}>Add / Edit Contact</Text>
          {/* <Image source={images} style={{width:100,height:100,borderRadius:50,backgroundColor:'red'}} /> */}

          <Image />
          <View
            style={{
              justifyContent: "center",
              alignItems: "flex-start",
              width: 170,
            }}
          >
            <TouchableOpacity
              onPress={openImagePickerAsync}
              style={{
                backgroundColor: "blue",
                height: 50,
                width: 50,
                borderRadius: 40,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text>
                <Icon
                  name="pen"
                  size={15}
                  backgroundColor="#ffffff"
                  color={"#ffff"}
                />
              </Text>
            </TouchableOpacity>
            {image && (
              <Image
                source={{ uri: image }}
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 50,
                  bottom: 50,
                  left: 40,
                }}
              />
            )}
          </View>
        </View>

        <View style={{ flex: 2 }}>
          <View style={{ marginLeft: 20 }}>
            <View>
              <Text style={{ color: "black" }}>Full name</Text>
            </View>
            <View>
              <TextInput
                placeholder=""
                style={{
                  width: 350,
                  height: 60,
                  top: 5,
                  borderRadius: 10,
                  borderWidth: 1,
                  borderColor: "black",
                  padding: 10,
                }}
                
              />
            </View>
          </View>

          <View style={{ marginLeft: 20, top: 20 }}>
            <View>
              <Text style={{ color: "black" }}>Phone number</Text>
            </View>
            <View>
              <TextInput
                placeholder=""
                style={{
                  width: 350,
                  height: 60,
                  top: 5,
                  borderRadius: 10,
                  borderWidth: 1,
                  borderColor: "black",
                  padding: 10,
                }}
              />
            </View>
          </View>

          <View style={{ marginLeft: 20, top: 50 }}>
            <View>
              <Text style={{ color: "black" }}>Nickname</Text>
            </View>

            <View
              style={{
                flexDirection: "row",
                width: 350,
                height: 60,
                top: 5,
                borderRadius: 10,
                borderWidth: 1,
                borderColor: "black",
              }}
            >
              <Icon
                name="bullseye"
                size={15}
                backgroundColor="#2d2d"
                color={"black"}
                style={{ top: 22, left: 10 }}
              />
              <TextInput
                placeholder=""
                style={{
                  width: 350,
                  height: 60,
                  top: 0,
                  padding: 20,
                }}
              />
            </View>
          </View>

          <Text style={{ marginLeft: 20, top: 70 }}>
            <View>
              <Text style={{ color: "black" }}>Location</Text>
            </View>

            <View>
              {/* <TextInput
                placeholder=""
                style={{
                  width: 350,
                  height: 60,
                  top: 5,
                  borderRadius: 10,
                  borderWidth: 1,
                  borderColor: "black",
                  padding: 10,
                }}
              /> */}
                {/* {location} */}
                <Text style={{
                  width: 350,
                  height: 60,
                  top: 5,
                  borderRadius: 10,
                  borderWidth: 1,
                  borderColor: "black",
                  padding: 10,
                }}>
                  {location ? location.coords.latitude : ''} , {location ? location.coords.longitude : ''}
                </Text>
            </View>
          </Text>

          <View
            style={{
              marginVertical: 90,
              marginHorizontal: 20,
              justifyContent: "flex-end",
              alignItems: "flex-end",
            }}
          >
            <View
              style={{
                width: 150,
                height: 50,
                backgroundColor: "blue",
                borderRadius: 10,
                borderWidth: 1,
                justifyContent: "center",
                alignItems: "center",
                top:10
              }}
            >
              <Text style={{ color: "white", fontSize: 17 }}>Add</Text>
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
};

export default App;
