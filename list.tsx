const array = [
  {
    id: 1,
    item: "Jim Halpert 1",
    email: "@halper",
    image: require("./img/images.png"),
  },
  {
    id: 2,
    item: "Jim Halpert 2",
    email: "@halper",
    image: require("./img/images.png"),
  },
  {
    id: 3,
    item: "Jim Halpert 3",
    email: "@halper",
    image: require("./img/images.png"),
  },
  {
    id: 4,
    item: "Jim Halpert 4",
    email: "@halper",
    image: require("./img/images.png"),
  },
  {
    id: 5,
    item: "Jim Halpert 5",
    email: "@halper",
    image: require("./img/images.png"),
  },
  {
    id: 6,
    item: "Jim Halpert 6",
    email: "@halper",
    image: require("./img/images.png"),
  },
  {
    id: 7,
    item: "Jim Halpert 7",
    email: "@halper",
    image: require("./img/images.png"),
  },
  {
    id: 8,
    item: "Jim Halpert 8",
    email: "@halper",
    image: require("./img/images.png"),
  },
  {
    id: 9,
    item: "Jim Halpert 9",
    email: "@halper",
    image: require("./img/images.png"),
  },
  {
    id: 10,
    item: "Jim Halpert 10",
    email: "@halper",
    image: require("./img/images.png"),
  },
];

export default array;
