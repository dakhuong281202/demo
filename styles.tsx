import React from "react";
import { StyleSheet } from "react-native";

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  top: {
    flex: 2,
  },
  Text: {
    top: 50,
    color: "black",
    fontSize: 24,
    left: 20,
  },
  input: {
    flex: 6,
  },
  input1: {
    width: 320,
  },
  input11: {
    borderRadius: 10,
    borderWidth: 1,
    padding: 10,
    height: 70,
    margin: 12,
    flexDirection: "row",
    justifyContent: "space-between",
  },

  input12: {
    borderRadius: 10,
    height: 70,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  ViewText: {
    left: "60%",
  },
  button: {
    top: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  abcd: {
    justifyContent: "center",
    alignItems: "center",
    width: 370,
    height: 70,
    backgroundColor: "blue",
    borderRadius: 10,
  },

  textStyle: {
    paddingHorizontal: 150,
    paddingVertical: 10,
    
    color: "white",
    fontSize: 20,
    
  },
  containersetting: {
    flex: 1,
    backgroundColor: "#eff7f8",
  },
  settingTop: {
    width: "100%",
    height: 80,
    backgroundColor: "white",
  },
  settingText: {
    fontSize: 25,
    left: 10,
    top: 10,
  },

  settingLanguage: {
    top: 30,
    backgroundColor: "white",
    height: 70,
    justifyContent: "center",
  },
  textLanguage: {
    left: 20,
    fontSize: 20,

    height: 50,
  },

  settingThemes: {
    top: 35,
    backgroundColor: "white",
    height: 70,
    justifyContent: "center",
    
  },
  textThemes: {
    left: 20,
    fontSize: 20,
    height: 50,
    justifyContent:'center',
    top:10,
    width:350,
  },

  settingNotifications: {
    top: 40,
    backgroundColor: "white",
    height: 70,
    justifyContent: "center",
  },
  textNotifications: {
    left: 20,
    fontSize: 20,
    height: 50,
    justifyContent:'center',
    top:10,
    width:350,
  },
  settingSignOut: {
    top: 70,
    backgroundColor: "white",
    height: 70,
    justifyContent: "center",
  },
  textSignOut: {
    left: 20,
    fontSize: 20,
    height: 50,
    
  },

  containerContac: {
    flex: 1,
    backgroundColor:'black',
    
    
  },
  abc: {
    flexDirection: "row",
    padding:10,
    top: 20,
    
  },
  ContacTop: {
    flex: 1,
    backgroundColor: "white",
    
    
  },
  TextContac: {
    fontSize: 28,
    left: 15,
    top: 10,
    fontWeight: "bold",
    
  },
  btnContacs: {
    fontSize: 20,
    color: "blue",
    left:20,
    
  },
  ListContacs: {
    flex: 4,
  
    
  },
  ContainerModrdata: {
    flex: 1,
    backgroundColor: "white"
    
    
  },
  MoredataText: {
    flex: 1,
    
  },
  MdtText: {
    fontSize: 20,
    paddingHorizontal: 20,
    paddingVertical: 10,
    
    fontWeight: "bold",
  },
  Forgot:{
    backgroundColor:'white',
    flex:1
  }
});

export default style;
